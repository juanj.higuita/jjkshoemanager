package proyecto.controller;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import proyecto.bsn.ProductoBsn;
import proyecto.bsn.exception.ProductoYaExisteException;
import proyecto.dao.impl.ProductoDAONIO;
import proyecto.model.Producto;

import java.io.IOException;
import java.util.List;

public class RegistrarProductoController {

    @FXML
    private BorderPane contenedorProducto;
    @FXML
    private TextField txtId;
    @FXML
    private TextField txtTalla;
    @FXML
    private TextField txtMarca;
    @FXML
    private TextField txtValor;
    @FXML
    private TextField txtGenero;

    @FXML
    private TableView<Producto> tblProductos;
    @FXML
    private TableColumn<Producto, String> clmId;
    @FXML
    private TableColumn<Producto, String> clmTalla;
    @FXML
    private TableColumn<Producto, String> clmMarca;
    @FXML
    private TableColumn<Producto, String> clmValor;
    @FXML
    private TableColumn<Producto, String> clmGenero;

    private ProductoBsn productoBsn;
    private ProductoDAONIO productoDAONIO;

    public RegistrarProductoController() {
        productoBsn = new ProductoBsn();
    }

    public ProductoBsn getProductoBsn() {
        return productoBsn;
    }

    @FXML
    public void initialize(){
        clmId.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getId()));
        clmTalla.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getTalla()));
        clmMarca.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getMarca()));
        clmValor.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getValor()));
        clmGenero.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getGenero()));
        refrescarTabla();
    }

    public void btnVenta_action() throws IOException{
        String idIngresado = txtId.getText().trim();
        String tallaIngresada = txtTalla.getText().trim();
        String marcaIngresada = txtMarca.getText().trim();
        String valorIngresado = txtValor.getText().trim();
        String generoIngresado = txtGenero.getText().trim();
        marcaIngresada = marcaIngresada.toLowerCase();
        generoIngresado = generoIngresado.toUpperCase();

        if (idIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de producto", "Resultado de la transacción", "El id es requerido");
            txtId.requestFocus();
            return;
        }

        try {
            Integer.parseInt(idIngresado);
        } catch (NumberFormatException numberFormatException) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de producto", "Resultado de la transacción", "El id debe ser un valor numérico");
            txtId.requestFocus();
            txtId.clear();
            return;
        }

        try {
            Double.parseDouble(tallaIngresada);
        } catch (NumberFormatException numberFormatException) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de producto", "Resultado de la transacción", "La talla debe ser un valor numérico");
            txtTalla.requestFocus();
            txtTalla.clear();
            return;
        }

        if (tallaIngresada.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de producto", "Resultado de la transacción", "Los nombres son requeridos");
            txtTalla.requestFocus();
            return;
        }

        if (marcaIngresada.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de producto", "Resultado de la transacción", "La marca es requerida");
            txtMarca.requestFocus();
            return;
        }

        try {
            Integer.parseInt(valorIngresado);
        } catch (NumberFormatException numberFormatException) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de producto", "Resultado de la transacción", "El valor debe ser un valor numérico");
            txtValor.requestFocus();
            txtValor.clear();
            return;
        }

        if (valorIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de producto", "Resultado de la transacción", "El valor es requerido");
            txtValor.requestFocus();
            return;
        }

        if (!generoIngresado.equals("M")  && !generoIngresado.equals("F")) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de producto", "Resultado de la transacción", "El genero debe ser una de las letras indicadas");
            txtGenero.requestFocus();
            txtGenero.clear();
            return;
        }

        if (generoIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de producto", "Resultado de la transacción", "El genero es requerido");
            txtGenero.requestFocus();
            return;
        }

        btnEliminar_action();
        refrescarTabla();
        mostrarMensaje(Alert.AlertType.INFORMATION, "Venta de producto", "Resultado de la transacción", "Por favor culmine la operación");
        Parent ventanaRegistroVenta = FXMLLoader.load(getClass().getResource("../view/registrar-venta.fxml"));
        this.contenedorProducto.setCenter(ventanaRegistroVenta);
    }

    public void btnGuardar_action() {
        String idIngresado = txtId.getText().trim();
        String tallaIngresada = txtTalla.getText().trim();
        String marcaIngresada = txtMarca.getText().trim();
        String valorIngresado = txtValor.getText().trim();
        String generoIngresado = txtGenero.getText().trim();
        marcaIngresada = marcaIngresada.toLowerCase();
        generoIngresado = generoIngresado.toUpperCase();

        if (idIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de producto", "Resultado de la transacción", "El id es requerido");
            txtId.requestFocus();
            return;
        }

        try {
            Integer.parseInt(idIngresado);
        } catch (NumberFormatException numberFormatException) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de producto", "Resultado de la transacción", "El id debe ser un valor numérico");
            txtId.requestFocus();
            txtId.clear();
            return;
        }

        try {
            Double.parseDouble(tallaIngresada);
        } catch (NumberFormatException numberFormatException) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de producto", "Resultado de la transacción", "La talla debe ser un valor numérico");
            txtTalla.requestFocus();
            txtTalla.clear();
            return;
        }

        if (tallaIngresada.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de producto", "Resultado de la transacción", "Los nombres son requeridos");
            txtTalla.requestFocus();
            return;
        }

        if (marcaIngresada.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de producto", "Resultado de la transacción", "La marca es requerida");
            txtMarca.requestFocus();
            return;
        }

        try {
            Integer.parseInt(valorIngresado);
        } catch (NumberFormatException numberFormatException) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de producto", "Resultado de la transacción", "El valor debe ser un valor numérico");
            txtValor.requestFocus();
            txtValor.clear();
            return;
        }

        if (valorIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de producto", "Resultado de la transacción", "El valor es requerido");
            txtValor.requestFocus();
            return;
        }

        if (!generoIngresado.equals("M")  && !generoIngresado.equals("F")) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de producto", "Resultado de la transacción", "El genero debe ser una de las letras indicadas");
            txtGenero.requestFocus();
            txtGenero.clear();
            return;
        }

        if (generoIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de producto", "Resultado de la transacción", "El genero es requerido");
            txtGenero.requestFocus();
            return;
        }
        Producto producto = new Producto(idIngresado, tallaIngresada, marcaIngresada, valorIngresado, generoIngresado);
        List<Producto> productosList = productoBsn.consultarProductos();

        refrescarTabla();
        if(productosList.contains(producto)){
            mostrarMensaje(Alert.AlertType.ERROR, "Consulta de producto", "Resultado de la transacción", "El producto ya está registrado");
            return;
        }

        try {
            this.productoBsn.registrarProducto(producto);
            mostrarMensaje(Alert.AlertType.INFORMATION, "Registro de producto", "Resultado de la transacción", "El producto ha sido registrado con éxito");
            limpiarCampos();
            Producto productoClon = producto.clone();
            tblProductos.getItems().add(productoClon);
        } catch (ProductoYaExisteException e) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de producto", "Resultado de la transacción", e.getMessage());
        } catch (CloneNotSupportedException cnse){
            this.refrescarTabla();
        }
    }

    public void btnEliminar_action() throws IOException {
        String idIngresado = txtId.getText().trim();
        String tallaIngresada = txtTalla.getText().trim();
        String marcaIngresada = txtMarca.getText().trim();
        String valorIngresado = txtValor.getText().trim();
        String generoIngresado = txtGenero.getText().trim();
        marcaIngresada = marcaIngresada.toLowerCase();
        generoIngresado = generoIngresado.toUpperCase();

        if (idIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de producto", "Resultado de la transacción", "El id es requerido");
            txtId.requestFocus();
            return;
        }

        try {
            Integer.parseInt(idIngresado);
        } catch (NumberFormatException numberFormatException) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de producto", "Resultado de la transacción", "El id debe ser un valor numérico");
            txtId.requestFocus();
            txtId.clear();
            return;
        }

        Producto producto = new Producto(idIngresado, tallaIngresada, marcaIngresada,valorIngresado,generoIngresado);

        String productoString = producto.getId() + "," + producto.getTalla() + "," + producto.getMarca() + "," + producto.getValor() + "," + producto.getGenero();

        List<Producto> productosList = productoBsn.consultarProductos();

        if (productosList.contains(producto)) {
            ProductoDAONIO.removeLine(productoString);
            mostrarMensaje(Alert.AlertType.INFORMATION, "Eliminación de producto", "Resultado de la transacción", "El producto ha sido eliminado");
            refrescarTabla();
            limpiarCampos();
        } else{
            mostrarMensaje(Alert.AlertType.ERROR, "Eliminación de producto", "Resultado de la transacción", "El producto no está registrado");
            refrescarTabla();
            limpiarCampos();
        }

        refrescarTabla();
        limpiarCampos();
    }

    public void btnConsultar_action() {
        String idIngresado = txtId.getText().trim();
        String tallaIngresada = txtTalla.getText().trim();
        String marcaIngresada = txtMarca.getText().trim();
        String valorIngresado = txtValor.getText().trim();
        String generoIngresado = txtGenero.getText().trim();

        Producto producto = new Producto(idIngresado, tallaIngresada, marcaIngresada, valorIngresado, generoIngresado);

        List<Producto> productosList = productoBsn.consultarProductos();

        refrescarTabla();
        if(productosList.contains(producto)){
            mostrarMensaje(Alert.AlertType.INFORMATION, "Consulta de producto", "Resultado de la transacción", "El producto ya está registrado");
        } else {
            mostrarMensaje(Alert.AlertType.ERROR, "Consulta de producto", "Resultado de la transacción", "El producto no está registrado");
        }

    }



    private boolean validarCampos(String... campos) {
        boolean sonValidos = true;
        for (String campo : campos) {
            if (campo == null || campo.trim().isEmpty()) {
                sonValidos = false;
                break;
            }
        }
        return sonValidos;
    }


    private void limpiarCampos() {
        this.txtId.clear();
        this.txtTalla.clear();
        this.txtMarca.clear();
        this.txtValor.clear();
        this.txtGenero.clear();
    }

    private void mostrarMensaje(Alert.AlertType tipo, String titulo, String encabezado, String mensaje) {
        Alert alert = new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(encabezado);
        alert.setContentText(mensaje);
        alert.showAndWait();
    }

    public void refrescarTabla(){
        List<Producto> productosList = productoBsn.consultarProductos();
        ObservableList<Producto> productosListObservable = FXCollections.observableList(productosList);
        tblProductos.setItems(productosListObservable);
    }
}
