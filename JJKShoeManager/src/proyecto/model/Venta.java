package proyecto.model;

public class Venta {
    String id;
    String fecha;
    String valor;
    String cliente;

    public Venta(String id, String fecha, String valor, String cliente) {
        this.id = id;
        this.fecha = fecha;
        this.valor = valor;
        this.cliente = cliente;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }


    @Override
    public Venta clone() throws CloneNotSupportedException {
        return (Venta) super.clone();
    }

    @Override
    public boolean equals(Object o) {
        if(o == this){
            return true;
        }
        if(!(o instanceof Venta)) {
            return false;
        }
        Venta vt = (Venta)o;
        if(vt.getId().equals(this.getId()) && !(vt.getFecha().equals(this.getFecha()) & !vt.getValor().equals(this.getValor()) & !vt.getCliente().equals(this.getCliente()))){
            return true;
        }
        if(vt.getId().equals(this.getId()) && vt.getFecha().equals(this.getFecha()) && vt.getValor().equals(this.getValor()) && vt.getCliente().equals(this.getCliente())){
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString(){
        return this.id.toString() + "," + this.fecha.toString() + "," + this.valor.toString() + this.cliente.toString();
    }
}