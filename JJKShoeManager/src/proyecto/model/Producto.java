package proyecto.model;

public class Producto {
    String id;
    String talla;
    String marca;
    String valor;
    String genero;

    public Producto(String id, String talla, String marca, String valor, String genero) {
        this.id = id;
        this.talla = talla;
        this.marca = marca;
        this.valor = valor;
        this.genero = genero;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTalla() {
        return talla;
    }

    public void setTalla(String talla) {
        this.talla = talla;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    @Override
    public Producto clone() throws CloneNotSupportedException {
        return (Producto) super.clone();
    }

    @Override
    public boolean equals(Object o) {
        if(o == this){
            return true;
        }
        if(!(o instanceof Producto)) {
            return false;
        }
        Producto pr = (Producto) o;
        if(pr.getId().equals(this.getId()) && !(pr.getTalla().equals(this.getTalla()) & !pr.getMarca().equals(this.getMarca()) & !pr.getValor().equals(this.getValor()) & !pr.getGenero().equals(this.getGenero()))){
            return true;
        }
        if(pr.getId().equals(this.getId()) && pr.getTalla().equals(this.getTalla()) && pr.getMarca().equals(this.getMarca()) && pr.getValor().equals(this.getValor()) && pr.getGenero().equals(this.getGenero())){
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString(){
        return this.id.toString() + "," + this.talla.toString() + "," + this.marca.toString() + this.valor.toString() + "," + this.genero.toString();
    }
}
