package proyecto.bsn.exception;

public class VentaYaExisteException extends Exception{

    public VentaYaExisteException() { super("Ya existe una venta con el id ingresado");
    }
}
