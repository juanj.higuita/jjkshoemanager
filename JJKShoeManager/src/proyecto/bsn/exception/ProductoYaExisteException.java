package proyecto.bsn.exception;

public class ProductoYaExisteException extends Exception{
    public ProductoYaExisteException() {

        super("Ya existe un producto con el id ingresado");
    }
}
