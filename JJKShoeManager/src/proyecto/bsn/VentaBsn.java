package proyecto.bsn;

import proyecto.bsn.exception.VentaYaExisteException;
import proyecto.dao.VentaDAO;
import proyecto.dao.impl.VentaDAONIO;
import proyecto.model.Venta;

import java.util.List;
import java.util.Optional;

public class VentaBsn {

    private VentaDAO ventaDAO;

    public VentaBsn() { this.ventaDAO = new VentaDAONIO(); }


    public void registrarVenta(Venta venta) throws VentaYaExisteException {
        Optional<Venta> ventaOptional = this.ventaDAO.consultarPorId(venta.getId());
        if (ventaOptional.isPresent()) {
            throw new VentaYaExisteException();
        } else {
            this.ventaDAO.registrarVenta(venta);
        }
    }
    public boolean consultarVenta(Venta venta)  {
        Optional<Venta> ventaOptional = this.ventaDAO.consultarPorId(venta.getId());
        if (ventaOptional.isPresent()) {
            return true;
        } else {
            return false;
        }
    }

    public List<Venta> consultarVentas() { return ventaDAO.consultarVentas(); }
}

