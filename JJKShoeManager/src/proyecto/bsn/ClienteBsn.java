package proyecto.bsn;

import proyecto.bsn.exception.ClienteYaExisteException;
import proyecto.bsn.exception.ProductoYaExisteException;
import proyecto.dao.ClienteDAO;
import proyecto.dao.impl.ClienteDAONIO;
import proyecto.model.Cliente;

import java.util.List;
import java.util.Optional;

public class ClienteBsn {

    private ClienteDAO clienteDAO;

    public ClienteBsn() { this.clienteDAO = new ClienteDAONIO(); }


    public void registrarCliente(Cliente cliente) throws ClienteYaExisteException {
        Optional<Cliente> clienteOptional = this.clienteDAO.consultarPorId(cliente.getId());
        if (clienteOptional.isPresent()) {
            throw new ClienteYaExisteException();
        } else {
            this.clienteDAO.registrarCliente(cliente);
        }
    }

    public boolean consultarCliente(Cliente cliente)  {
        Optional<Cliente> clienteOptional = this.clienteDAO.consultarPorId(cliente.getId());
        if (clienteOptional.isPresent()) {
            return true;
        } else {
            return false;
        }
    }

    public List<Cliente> consultarClientes() { return clienteDAO.consultarClientes(); }

}
