package proyecto.dao.impl;

import proyecto.dao.ClienteDAO;
import proyecto.model.Cliente;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static java.nio.file.StandardOpenOption.APPEND;

public class ClienteDAONIO implements ClienteDAO {

    private final static String NOMBRE_ARCHIVO = "clientes";
    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);

    private static final String FIELD_SEPARATOR = ",";
    private static final String RECORD_SEPARATOR = System.lineSeparator();

    public ClienteDAONIO() {
        if (!Files.exists(ARCHIVO)) {
            try {
                Files.createFile(ARCHIVO);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    @Override
    public void registrarCliente(Cliente cliente) {
        String registro = parseClienteString(cliente);
        byte[] datosRegistro = registro.getBytes();
        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
        try (FileChannel fc = (FileChannel.open(ARCHIVO, APPEND))) {
            fc.write(byteBuffer);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private String parseClienteString(Cliente cliente) {
        StringBuilder sb = new StringBuilder();
        sb.append(cliente.getId()).append(FIELD_SEPARATOR)
                .append(cliente.getNombre()).append(FIELD_SEPARATOR)
                .append(cliente.getTelefono()).append(RECORD_SEPARATOR);
        return sb.toString();
    }

    @Override
    public Optional<Cliente> consultarPorId(String id) {

        return Optional.empty();
    }

    @Override
    public List<Cliente> consultarClientes() {
        List<Cliente> clientes = new ArrayList<>();
        try (Stream<String> stream = Files.lines(ARCHIVO)) {
            stream.forEach(clienteString -> {
                Cliente clienteConvertido = parseClienteObject(clienteString);
                clientes.add(clienteConvertido);
            });
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return clientes;
    }


    public static void removeLine(String Line) throws IOException{

        BufferedReader br = new BufferedReader(new FileReader(ARCHIVO.toString()));
        File temp = new File("temp.txt");
        BufferedWriter bw = new BufferedWriter(new FileWriter(temp));
        String removeID = Line;
        String currentLine;
        while((currentLine = br.readLine()) != null){
            String trimmedLine = currentLine.trim();
            if(trimmedLine.equals(removeID)){
                System.out.println("entra :)");
                currentLine = "";

            }
            if(!currentLine.isEmpty()){
                bw.write(currentLine + System.getProperty("line.separator"));
                System.out.println("entra pero fuera xd");
            }

        }
        bw.close();
        br.close();
        boolean delete = ARCHIVO.toFile().delete();
        boolean b = temp.renameTo(ARCHIVO.toFile());

        System.out.println(delete);
        System.out.println(b);
    }



    private Cliente parseClienteObject(String clienteString) {
        String[] datosCliente = clienteString.split(FIELD_SEPARATOR);
        Cliente cliente = new Cliente(datosCliente[0], datosCliente[1],
                datosCliente[2]);
        return cliente;
    }
}
