package proyecto.dao;

import proyecto.model.Producto;
import java.util.List;
import java.util.Optional;

public interface ProductoDAO {

    void registrarProducto (Producto estudiante);

    Optional<Producto> consultarPorId(String id);

    List<Producto> consultarProductos();


}
