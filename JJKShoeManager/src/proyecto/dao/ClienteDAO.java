package proyecto.dao;

import proyecto.model.Cliente;
import java.util.List;
import java.util.Optional;

public interface ClienteDAO {

    void registrarCliente (Cliente cliente);

    Optional<Cliente> consultarPorId(String id);

    List<Cliente> consultarClientes();

}
